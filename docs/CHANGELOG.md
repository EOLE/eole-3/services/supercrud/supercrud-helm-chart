# Changelog

### [1.2.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/compare/release/1.2.0...release/1.2.1) (2024-10-08)


### Bug Fixes

* deploy appVersion to 1.4.0 ([73e727b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/73e727b834d491780d6701185d71a459895f2a89))
* dev helm deploys dev image ([3dda5c5](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/3dda5c5fc998094d9ec38c9efa1f7c461774f4de))

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/compare/release/1.1.0...release/1.2.0) (2024-06-13)


### Features

* deploy supercrud 1.3.0 ([2237b19](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/2237b19abf12388b65cf031bc10886dd3de23d96))


### Bug Fixes

* dev helm deploys dev image ([23f6df6](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/23f6df68f4a7b0c237663ac0b1b693cb5159ab4e))
* testing helm deploys testing image ([78ac4bf](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/78ac4bf522474190a3572de5047bd6c18d0f30ae))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/compare/release/1.0.1...release/1.1.0) (2023-10-17)


### Features

* new helm stable version for app version 1.2.0 ([1583359](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/1583359c134444e71ea78f4e2865bd56cda13642))

### [1.0.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/compare/release/1.0.0...release/1.0.1) (2023-10-05)


### Bug Fixes

* dev chart deploy dev appversion ([b55aecb](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/b55aecbe43474a70a00eef67af4985bf5ba82319))
* remove default image.tag from values.yaml ([30a0869](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/30a08695d0354638292c71e5f8b7f98ded84389e))

## 1.0.0 (2023-10-05)


### Features

* **ci:** build helm package ([d868199](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/d868199f7304554187cc678e35bc7ae5473a74ac))
* **ci:** push helm package to chart repository ([c347186](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/c347186735a52e75786cfad850705e4328a8b3cd))
* **ci:** update helm chart version with `semantic-release` ([48d2270](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/48d2270cae3fb10cf54cb1105d0d6d643638976c))
* **ci:** validate helm chart at `lint` stage ([dd2543c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/dd2543c614cf96189f643305e190d011087f1f7a))
* first stable version for supercrud 1.1.0 ([98b339e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/98b339e2f75a6f53303e3261c02417ec9f91b24e))
* publish testing version of supercrud ([94f2af2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/94f2af2fbec26c56c148e1daf792d5bb98d8c30f))
* update helm with new version of supercrud ([55947ed](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/55947ed0ae79bbcf1c80a3ad16aaa76b7f50b26e))


### Continuous Integration

* **commitlint:** enforce commit message format ([82bf37a](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/82bf37ac7ee4e227641c0f7204609d4d53d362e1))
* **release:** create release automatically with `semantic-release` ([79020ce](https://gitlab.mim-libre.fr/EOLE/eole-3/services/supercrud/supercrud-helm-chart/commit/79020cee906f5ff8960e8cca7599c8fdc0770df0))
